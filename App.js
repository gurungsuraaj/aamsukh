/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar } from 'react-native';
import MainNavigation from './Navigation';
import { Provider } from 'react-redux';
import {createStore, compose,applyMiddleware} from 'redux';
import logger from "redux-logger"


const initialState = {
  orders: [],
  // allDetails: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ORDERS':
        console.log('action', action);
        return{
           orders: [...state.orders, action.data]
        }
    // case 'ADD_WHOLE_DETAIL':
    //     return{
    //       allDetails: [...state.allDetails,action.wholedata]
    //     }
      
    case 'RESET_ORDERS':
      return { orders : state.orders = [] };

    case 'DELETE_ORDERS': 
     return {
       state
     }
  }
  return state
};


const configureStore = compose(applyMiddleware(logger)(createStore)(reducer));



// const store = createStore(reducer);

export default class App extends Component {
  render() {
    return (
      <Provider store={configureStore}>
        <MainNavigation />
      </Provider>
    );
  }
}




