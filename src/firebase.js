import * as firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyCzepacw8zLi1UxB9QCFHxiS7whidOfTyE",
    authDomain: "aamsukh-d5190.firebaseapp.com",
    databaseURL: "https://aamsukh-d5190.firebaseio.com",
    projectId: "aamsukh-d5190",
    storageBucket: "aamsukh-d5190.appspot.com",
    messagingSenderId: "778036557892",
    appId: "1:778036557892:web:5cea74ac59d04ac1" }
  
  export default !firebase.apps.length ?firebase.initializeApp(firebaseConfig): firebase.app();