import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet, Dimensions, Button, Alert } from 'react-native';
import { TextInput } from "react-native-paper";
const { width, height } = Dimensions.get("window");
import { connect } from 'react-redux';

import Icon from "react-native-vector-icons/Ionicons";



class AddOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dress: '',
      design:'',
      la: '',
      xa: '',
      ka: '',
      dhatal: '',
      motha: '',
      gaala: '',
      extra: '',
      
    };
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params = {} } = navigation.state;
    return {
      title: "Add Orders",
      // headerStyle: {
      //    backgroundColor: '#0072C6',
      // },
      headerTintColor: 'black',
      headerTitleStyle: {
        fontWeight: "500",
        fontSize: 18
      }
    }
  }

 


  addNewOrder = () => {
    const data = {
      dress: this.state.dress,
      design: this.state.design,
      la: this.state.la,
      xa: this.state.xa,
      ka: this.state.ka,
      dhatal: this.state.dhatal,
      motha: this.state.motha,
      gaala: this.state.gaala,
      extra: this.state.extra,
  
    }

    Alert.alert("Order Saved");

    this.props.addOrders(data);
  }

  addNextOrder =() =>{
    this.setState({
      dress: '',
      design:'',
      la: '',
      xa: '',
      ka: '',
      dhatal: '',
      motha: '',
      gaala: '',
      extra: '',
    })

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View>
            <Text style={{ paddingTop: 12, marginLeft: 22 }}>Dress</Text>
            <Picker
              selectedValue={this.state.dress}
              style={{ height: 50, width: 150, marginLeft: 24 }}
              onValueChange={(dress, itemIndex) =>
                this.setState({ dress: dress })
              }
            >
              <Picker.Item label="Kurtha" value="Kurtha" />
              <Picker.Item label="Blouse" value="Blouse" />
              <Picker.Item label="Lehenga" value="Lehenga" />
              <Picker.Item label="Circular Skirt" value="Circular Skirt" />
              <Picker.Item label="Bridal" value="Bridal" />
            </Picker>
          </View>

          <View>
            <Text style={{ paddingTop: 12, marginLeft: 22 }}>Design</Text>
            <Picker
              selectedValue={this.state.design}
              style={{ height: 50, width: 150, marginLeft: 24 }}
              onValueChange={(design, itemIndex) =>
                this.setState({ design: design })
              }
            >
              <Picker.Item label="Boat" value="Boat" />
              <Picker.Item label="V-Shape" value="V-Shape" />
              <Picker.Item label="Mandir" value="Mandir" />
            </Picker>
          </View>
        </View>



        <View style={{ flexDirection: "row", justifyContent: 'space-between', paddingHorizontal: 10 }}>
          <TextInput
            value={this.state.la}
            onChangeText={la => {
              this.setState({ la });
            }}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput}
            label="ल "
            keyboardType={"number-pad"}
          /><TextInput
            value={this.state.xa}
            onChangeText={xa => {
              this.setState({ xa });
            }}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput}
            label="छ"
            keyboardType={"number-pad"}
          />

        </View>

        <View style={{ flexDirection: "row", justifyContent: 'space-between', paddingHorizontal: 10 }}>
          <TextInput
            value={this.state.ka}
            onChangeText={ka => {
              this.setState({ ka });
            }}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput}
            label="MOTHA"
            keyboardType={"number-pad"}
          /><TextInput
            value={this.state.dhatal}
            onChangeText={dhatal => {
              this.setState({ dhatal });
            }}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput}
            label="GAALA"
            keyboardType={"number-pad"}
          />

        </View>


        <View style={{ flexDirection: "row", justifyContent: 'space-between', paddingHorizontal: 10 }}>
          <TextInput
            value={this.state.motha}
            onChangeText={motha => {
              this.setState({ motha });
            }}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput}
            label="मोथा"
            keyboardType={"number-pad"}
          /><TextInput
            value={this.state.gaala}
            onChangeText={gaala => {
              this.setState({ gaala });
            }}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput}
            label="गला"
            keyboardType={"number-pad"}
          />

        </View>
       

        <TextInput
          value={this.state.extra}
          onChangeText={extra => {
            this.setState({ extra });
          }}
          theme={{ colors: { primary: "#0072C6" } }}
          underlineColor="#DDDDDD"
          style={[styles.textInput, { width: width * 0.95 }]}
          label="Extra Details"
          multiline={true}
          numberOfLines={4}
        />

        <View style={{
          flex: 1,
          justifyContent: 'flex-end',
          marginBottom: 36
        }}>
          <View style={{ margin: 20, borderRadius: 10, justifyContent: 'space-evenly', flexDirection: 'row' }}>
            <Button
              title="Save Details"
              color="#0072C6"
              accessibilityLabel="Learn more about this purple button"
              style={{ borderRadius: 5, margin: 24 }}
              onPress={this.addNewOrder}

            />
            <Button
              title="Add Next Order"
              color="#0072C6"
              accessibilityLabel="Learn more about this purple button"
              style={{ borderRadius: 5, margin: 24 }}
              onPress={this.addNextOrder}

            />
          </View>
        </View>

      </View>
    );
  }
}




function mapDispatchToProps(dispatch) {
  return {
    addOrders: (data) => dispatch({ type: 'ADD_ORDERS', data })
  };
}

export default connect(
  null,
  mapDispatchToProps
)(AddOrders);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e8eaed'
  },
  textInput: {
    backgroundColor: "#fff",
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 14,
    color: "#0072C6",
    width: width * 0.35,
  }
});