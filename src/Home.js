import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert, ScrollView, Modal, Dimensions, RefreshControl, ActivityIndicator } from 'react-native';
import firebase from './firebase';
import { FAB } from 'react-native-paper';
const { height, width } = Dimensions.get('window');


export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firebaseData: [],
      result: '',
      modalVisible: false,
      selectedItem: [],
      refreshing: false,
      isloading: false,

    };
  }


  _onRefresh = () => {
    this.dataLoading();
  }

  setModalVisble(visible, item) {
    this.setState({
      modalVisible: visible,
      selectedItem: item,
    });
  }

  handleCloseModal() {
    this.setState({ modalVisible: false });
  }



  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params = {} } = navigation.state;
    return {
      title: "Aamsukh Orders Detail",
      // headerStyle: {
      //    backgroundColor: '#0072C6',
      // },
      headerTintColor: 'black',
      headerTitleStyle: {
        fontWeight: "500",
        fontSize: 18
      }
    }
  }


  componentDidMount() {
    this.dataLoading();
  }

  dataLoading() {
    this.setState({ refreshing: true });
    let firebasekofinaldata = [];
    fetch("https://aamsukh-d5190.firebaseio.com/CustomerList.json")
      .catch(err => {
        Alert.alert('Something went wrong');
        console.log(err);
    this.setState({ refreshing: false });
    


      }).then(res => res.json())
      .then(parsedRes => {
        let keys = Object.keys(parsedRes);
        console.log("keys", keys);
        keys.forEach((key) => {
          console.log("parsed", parsedRes[key])

          //firebasekofinaldata = [...firebasekofinaldata, parsedRes[key]]
          firebasekofinaldata.push(parsedRes[key])
        })

        console.log("final data", firebasekofinaldata)
        this.setState({
          firebaseData: firebasekofinaldata,
          refreshing: false
        });
        // console.log(this.state.firebaseData)
      })
  }



  render() {
    const data = this.state.firebaseData;
    const selectedData = this.state.selectedItem

    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
        />
      } >



        {this.state.isloading && <ActivityIndicator animating={this.state.isloading} size="large" color="blue" />}


        {
          data.map((item, index) =>
            <View>
              <TouchableOpacity key={index} onPress={() => this.setModalVisble(true, item)} style={styles.buttonStyle}>
                <Text style={styles.textStyle}>{item.customerName}</Text>
                <Text style={styles.textStyle}>{item.selectedDate}</Text>
              </TouchableOpacity>
              <Modal
                visible={this.state.modalVisible}
                animationType="none"
                transparent={false}
                onRequestClose={() => this.handleCloseModal()}>
                {!this.state.selectedItem ? (
                  null
                ) : (
                    <ScrollView>
                      <View style={{ paddingHorizontal: 10, justifyContent: 'space-between', flexDirection: 'row', flex: 1, paddingTop: 5 }}>

                        <View style={{ marginHorizontal: 10, borderRadius: 4, marginVertical: 10, height: 35, justifyContent: 'center' }}>
                          <Text style={{ paddingLeft: 3, fontSize: 16, color: '#0c0c0c' }}>Customer's Name: </Text></View>
                        <View style={{ backgroundColor: "#b6bbc4", marginHorizontal: 10, borderRadius: 4, marginVertical: 10, width: width * 0.5, height: 35, justifyContent: 'center', }}>
                          <Text style={{ paddingLeft: 10, fontSize: 16, color: 'black' }}>suraj</Text>
                        </View>
                      </View>

                      <View style={{ paddingHorizontal: 10, justifyContent: 'space-between', flexDirection: 'row', flex: 1, marginTop: 2 }}>

                        <View style={{ marginHorizontal: 10, borderRadius: 4, height: 35, justifyContent: 'center' }}>
                          <Text style={{ paddingLeft: 3, fontSize: 16, color: '#0c0c0c' }}>Phone number: </Text></View>
                        <View style={{ backgroundColor: "#b6bbc4", marginHorizontal: 10, borderRadius: 4, width: width * 0.5, height: 35, justifyContent: 'center', }}>
                          <Text style={{ paddingLeft: 10, fontSize: 16, color: 'black' }}>9806644423</Text>
                        </View>
                      </View>

                      <View style={{ paddingHorizontal: 10, justifyContent: 'space-between', flexDirection: 'row', flex: 1, marginTop: 10 }}>

                        <View style={{ marginHorizontal: 10, borderRadius: 4, height: 35, justifyContent: 'center' }}>
                          <Text style={{ paddingLeft: 3, fontSize: 16, color: '#0c0c0c' }}>Location: </Text></View>
                        <View style={{ backgroundColor: "#b6bbc4", marginHorizontal: 10, borderRadius: 4, width: width * 0.5, height: 35, justifyContent: 'center', }}>
                          <Text style={{ paddingLeft: 10, fontSize: 16, color: 'black' }}>Pokhara</Text>
                        </View>
                      </View>
                      <View style={{ paddingHorizontal: 10, justifyContent: 'space-between', flexDirection: 'row', flex: 1, marginTop: 10 }}>

                        <View style={{ marginHorizontal: 10, borderRadius: 4, height: 35, justifyContent: 'center' }}>
                          <Text style={{ paddingLeft: 3, fontSize: 16, color: '#0c0c0c' }}>Delivery Date: </Text></View>
                        <View style={{ backgroundColor: "#b6bbc4", marginHorizontal: 10, borderRadius: 4, width: width * 0.5, height: 35, justifyContent: 'center', }}>
                          <Text style={{ paddingLeft: 10, fontSize: 16, color: 'black' }}>22/1/2019</Text>
                        </View>
                      </View>
                      <View style={styles.headerStyle}>
                        <Text style={styles.appHeader}>Customer's Orders</Text>
                      </View>
                    </ScrollView>
                  )}
              </Modal>
            </View>
          )
        }
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    color: 'black'

  },
  buttonStyle: {
    marginTop: 15, marginHorizontal: 15, borderRadius: 4, elevation: 1, height: 43, justifyContent: 'space-between', flexDirection: 'row', marginBottom: 1

  },
  textStyle: {
    fontSize: 17, paddingHorizontal: 20, color: 'black', paddingTop: 10
  },
  appHeader: {
    margin: 8,
    marginLeft: 14,
    fontWeight: "bold",
    color: "#737373"
  },
  headerStyle: {
    backgroundColor: "#e0e0e0", marginTop: 30
  },

});