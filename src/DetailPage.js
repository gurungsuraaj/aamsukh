import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Picker,
  Button, TouchableOpacity, ScrollView, Alert,ActivityIndicator
} from "react-native";
import { TextInput } from "react-native-paper";
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from 'react-redux';
import AddOrders from "./AddOrders";
import firebase from './firebase';

import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
const { width, height } = Dimensions.get("window");

class AddDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      customerName:"",
      phone:'',
      location:'',
      isloading: false,
      isDateTimePickerVisible: false,
      selectedDate: '',
      totalAmt:''

    };
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    this.setState({ selectedDate: moment(date).format('LL') });
    this.hideDateTimePicker();
  };

  static navigationOptions = ({ navigation, navigationOptions }) => {

    return {
      title: "Detail Screen",
      // headerStyle: {
      //   backgroundColor: '#0072C6',
      // },
      headerTintColor: 'black',
      headerTitleStyle: {
        fontWeight: "500",
        fontSize: 18
      },
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          {/* <TouchableOpacity onPress={() => navigation.navigate('AddCustomerDetail')} style={{ paddingRight: 25 }}>
            <Icon
              name="md-person-add"
              color="black"
              iconStyle={{ fontSize: 25, backgroundColor: "#0072C6" }}
              backgroundColor="#0072C6"
              borderRadius={50}
              size={30}

            />
          </TouchableOpacity> */}

          <TouchableOpacity onPress={() => navigation.navigate('AddOrders')} style={{ paddingRight: 25 }}>
            <Icon
              name="ios-add-circle-outline"
              color="black"
              iconStyle={{ fontSize: 25, backgroundColor: "#0072C6" }}
              backgroundColor="#0072C6"
              borderRadius={50}
              size={30}

            />
          </TouchableOpacity>

        </View>
      )
    }
  }


  details=()=>{
    // const wholedata = {
    //   customerName: this.state.customerName,
    //   phone: this.state.phone,
    //   location:this.state.location,
    //   orders: this.props.orders
    // }
    // this.props.addInfo(wholedata);
 
// console.log(wholedata);
this.setState({ isloading:true})
firebase.database().ref('CustomerList/').push({
  customerName: this.state.customerName,
  phone: this.state.phone,
  location:this.state.location,
  selectedDate: this.state.selectedDate,
  totalAmt:this.state.totalAmt,
  orders: this.props.orders
}).then((data)=>{
  //success callback
  Alert.alert("Customer Data saved Successfully!");
  console.log('data ' , data);
  this.setState({isloading: false,
    text: "",
    customerName:"",
    phone:'',
    location:'',
    isloading: false,
    selectedDate: '',
    totalAmt:''
  
  });
this.props.restOrders();

}).catch((error)=>{
  //error callback
  console.log('error ' , error)
})

  }

  render() {
    // const data = this.props.orders;
    return (
      <ScrollView style={styles.container}>
        <TextInput
          value={this.state.customerName}
          onChangeText={customerName => {
            this.setState({ customerName });
          }}
          theme={{ colors: { primary: "#0072C6" } }}
          underlineColor="#DDDDDD"
          style={styles.textInput}
          label="Customer Name"

        />

        <View style={{flexDirection:"row", justifyContent:'space-between'}}>
        <TextInput
          value={this.state.phone}
          onChangeText={phone => {
            this.setState({ phone });
          }}
          theme={{ colors: { primary: "#0072C6" } }}
          underlineColor="#DDDDDD"
          style={[styles.textInput,{width:width* 0.35}]}
          label="Phone Number"
          keyboardType={"number-pad"}
        />
        <TextInput
          value={this.state.location}
          onChangeText={location => {
            this.setState({ location });
          }}
          theme={{ colors: { primary: "#0072C6" } }}
          underlineColor="#DDDDDD"
          style={[styles.textInput,{width:width* 0.6}]}
          label="Location"
        />
        </View>

        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 10 }}>
          <TextInput
            value={this.state.selectedDate}
            theme={{ colors: { primary: "#0072C6" } }}
            underlineColor="#DDDDDD"
            style={styles.textInput2}
            label="Date"
          />
          <View
            style={{
              backgroundColor: "#0072C6",
              justifyContent: "center",
              alignItems: "center",
              width: width * 0.19,
              height: 46,
              borderRadius: 3,
              marginRight: 70
              
          
            }}
          >
            <Icon.Button
              name="md-calendar"
              color="#4CAF50"
              iconStyle={{ fontSize: 29, color: "white" }}
              backgroundColor="#0072C6"
              borderRadius={5}
              onPress={this.showDateTimePicker}
            />

          </View>
        </View>
        <TextInput
          value={this.state.totalAmt}
          onChangeText={totalAmt => {
            this.setState({ totalAmt });
          }}
          theme={{ colors: { primary: "#0072C6" } }}
          underlineColor="#DDDDDD"
          style={styles.textInput}
          label="Total Amount"
        />
         
        <View style={styles.headerStyle}>
          <Text style={styles.appHeader}>Customer's Orders</Text>
        </View>



        {this.props.orders.map((item, index) =>
          <TouchableOpacity key={index} >
            <View style={{
              marginTop: 15,paddingBottom:0, marginHorizontal: 10, borderRadius: 4, elevation: 1, height: 43, justifyContent: 'center',
            }}>
              <Text style={{ fontWeight: 'bold', fontSize: 16, paddingLeft: 20 }}>{item.dress}</Text>
            </View>
          </TouchableOpacity>
        )}

{this.state.isloading &&  <ActivityIndicator animating={this.state.isloading} size="large" color="blue" /> }

{
  this.props.orders.length >0 ? <View style={{
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 20
  }}>
    <View style={{ margin: 20, borderRadius: 10 }}>
      <Button
        title="Save All Details"
        color="#0072C6"
        accessibilityLabel="Learn more about this purple button"
        style={{ borderRadius: 5, margin: 24 }}
        onPress={this.details}

      />
    </View>
  </View> : null
}
        
<DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fefefe'
  },
  appHeader: {
    margin: 8,
    marginLeft: 14,
    fontWeight: "bold",
    color: "#737373"
  },
  textInput: {
    backgroundColor: "#fff",
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 14,
    color: "#0072C6"
  },
  textStyle: {
    paddingLeft: 15, marginVertical: 10, fontWeight: 'bold'
  },
  viewTextStyle: {
    backgroundColor: '#C5CCD6', height: 35, width: width * 0.9, borderRadius: 5, justifyContent: 'center', marginLeft: 15
  },
  headerStyle: {
    backgroundColor: "#e0e0e0", marginTop: 30
  },
  textInsideBox: {
    paddingLeft: 10, color: "#525257"
  },
  textInput2: {
    backgroundColor: "#fff",
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 14,
    color: "#0072C6",
    width: width * 0.35,
  }
});

function mapStateToProps(state) {
  return {
    orders: state.orders,

  };
}

function mapDispatchToProps(dispatch) {
  return {
    restOrders: () => dispatch({ type: 'RESET_ORDERS'  })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddDetail);