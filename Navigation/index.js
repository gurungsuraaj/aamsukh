import React, { Component } from "react";
import { View, Text,StyleSheet } from "react-native";
import {
  createBottomTabNavigator,
  createAppContainer,
  createStackNavigator
} from "react-navigation";
import DetailPage from "../src/DetailPage";
import Home from "../src/Home";
import Icon from "react-native-vector-icons/Ionicons";
import AddOrders from '../src/AddOrders';


const StackNav = createStackNavigator({
  
  DetailPage,
  AddOrders
    
})

const StackNav2 = createStackNavigator({
 Home
})

const MainNavigation = createBottomTabNavigator({
  Homes: {
    screen: StackNav2,
    navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-home" color={tintColor} size={24} />
        ),
        tabBarOptions: { activeTintColor:'black',  showLabel: false }
      }
  },
  Detailpage: {
    screen: StackNav,
    navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-contact" color={tintColor} size={24} />
        ),
        tabBarOptions: { activeTintColor:'black', showLabel: false }
      }
  },
  
});




export default createAppContainer(MainNavigation);
